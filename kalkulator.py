
def calculator (num1, num2, operator):
    if operator == "+":
        return (num1 + num2)
    elif operator == "-":
        return (num1 - num2)
    elif operator == "*":
        return (num1 * num2)
    elif operator == "/":
        return (num1 / num2)
    elif operator == "^":
        if num2 == 2:
            return (num1*num1)
        elif num2 == 3:
            return (num1*num1*num1)
        elif num2 == 4:
            return (num1*num1*num1*num1)
        else:
            print("potegowanie tylko do 4 potegi")
    else:
        print("podaj dobry operator")

num1 = float(input("give first number: "))
num2 = float(input("give second number: "))
operator = input("give operator (+, -, *, /, ^): ")

print(calculator(num1, num2, operator))